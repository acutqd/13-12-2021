  // truyền vào tham số câu hỏi
  function Quiz(questions) {
    this.score = 0; // điểm
    this.questions = questions; // câu hỏi là than số truyền vào
    this.questionIndex = 0; // câu hỏi bắt đâu ở vị trí đầu tiên
    // console.log(this.questions.length);
}

// lấy câu hỏi đầu tiên
Quiz.prototype.getQuestionIndex = function () {
    return this.questions[this.questionIndex];
}

// đáp ăn ( nếu đáp án đúng trả về true => điểm cộng 1, rôi chuyển sang câu hỏi tiếp theo)
Quiz.prototype.guess = function (answer, button) {
    // console.log(this.getQuestionIndex().isCorrectAnswer(answer));
    // console.log(button.id)
    // console.log(answer)
    if (this.getQuestionIndex().isCorrectAnswer(answer)) {

        this.score++;
    }
    // console.log(this.score);

    this.questionIndex++;
}

// kiểm tra xem đã hết cau hỏi hay chưa.. /trả về số lượng câu hỏi
Quiz.prototype.isEnded = function () {

    return this.questionIndex === this.questions.length;

}

// câu hỏi  có 3 tham số truyền vào: text => câu hỏi, choices => lựa chọn, answer =. đáp án đúng
// var question = object.
function Question(text, choices, answer) {
    // console.log(this.choices = choices);

    this.text = text;
    this.choices = choices;
    this.answer = answer;
}
// function Question(text, choices, answer) {
//     // console.log(choices);

//     this.text = text;
//     this.choices = choices;
//     this.answer = answer;
// }

// tạo hàm return đáp án để check ở hàm guess
Question.prototype.isCorrectAnswer = function (choice) {
    // document.ge
    // console.log(this.answer)
    return this.answer === choice;
}

// hiện thị câu hỏi
function populate() {
    if (quiz.isEnded()) { // nếu hết câu hỏi => show số điểm
        showScores();
    } else { // else
        // hiển thị câu hỏi
        var element = document.getElementById("question"); // lấy element để truyềng giá trị
        element.innerHTML = quiz.getQuestionIndex().text;

        // show options
        var choices = quiz.getQuestionIndex().choices;
        // console.log(choices)
        // lấy element để truyềng giá trị
        element.innerHTML = quiz.getQuestionIndex().text;
        // chạy vòng lăp mảng đáp án

        var element1 = document.getElementById("answer");
        // console.log(element1)


        for (var i = 0; i < choices.length; i++) {
            var element = document.getElementById("choice" + i);
            var abc ="hihi "
            if(choices[i] != ''){
                // element1.innerHTML = abc + choices[i]
                element.innerHTML = choices[i];
                guess("btn" + i, choices[i]);
            }
            // goi hàm guess ( có 2 tham số truyền vào là id  button đáp án và id đáp án)
        }
        // gọi hàm hiện câu hỏi
        showProgress();
    }
};

function guess(id, guess) {
    var button = document.getElementById(id);
    button.onclick = function () {
        // console.log(button)
        quiz.guess(guess);
        populate();
    }
};
function showProgress() {
    var currentQuestionNumber = quiz.questionIndex + 1;
    var element = document.getElementById("progress");
    element.innerHTML = "Question " + currentQuestionNumber + " of " + quiz.questions.length;
};

function showScores() {
    var gameOverHTML = "<h1>Result</h1>";
    gameOverHTML += "<h2 id='score'> Your scores: " + quiz.score + "</h2>";
    var element = document.getElementById("quiz");
    element.innerHTML = gameOverHTML;
};

// var questions = [];
// fetch("js/questions.json").then((respon) => respon.json()).then(respos => {
//     respos.forEach(element => {

//         var result = Object.keys(element['answers']).map(function (key) {
//             return element['answers'][key];
//         });
      
//         questions.push('hi');

//     });
// });

var json = [{
    "id": 1071,
    "question": "From which file does the command `free` takes it's information",
    "description": null,
    "answers": {
        "answer_a": "answer_a_correct",
        "answer_b": "\/proc\/freemem",
        "answer_c": "\/proc\/memfree",
        "answer_d": "\/dev\/meminfo",
        "answer_e": "\/dev\/freemem",
        "answer_f": "\/dev\/memfree"
    },
    "multiple_correct_answers": "false",
    "correct_answers": {
        "answer_a_correct": "true",
        "answer_b_correct": "false",
        "answer_c_correct": "false",
        "answer_d_correct": "false",
        "answer_e_correct": "false",
        "answer_f_correct": "false"
    },
    "correct_answer": "answer_a",
    "explanation": null,
    "tip": null,
    "tags": [{
        "name": "Linux"
    }],
    "category": "Linux",
    "difficulty": "Easy"
}, {
    "id": 26,
    "question": "What is a daemon?",
    "description": null,
    "answers": {
        "answer_a": "It is a generic name for e-mail servers on Linux. The most famous one is mailer-daemon",
        "answer_b": "It is a program that keeps running on the background after it is called, answering to requests done by users and other programs.",
        "answer_c": "It is an antivirus for Linux.",
        "answer_d": "It is the generic name for any Linux server.",
        "answer_e": null,
        "answer_f": null
    },
    "multiple_correct_answers": "false",
    "correct_answers": {
        "answer_a_correct": "false",
        "answer_b_correct": "true",
        "answer_c_correct": "false",
        "answer_d_correct": "false",
        "answer_e_correct": "false",
        "answer_f_correct": "false"
    },
    "correct_answer": "answer_b",
    "explanation": null,
    "tip": null,
    "tags": [{
        "name": "Linux"
    }],
    "category": "Linux",
    "difficulty": "Medium"
}, {
    "id": 665,
    "question": "Choose the correct usage of \u2018cd\u2019 to move into the parent directory.",
    "description": null,
    "answers": {
        "answer_a": "cd",
        "answer_b": "cd ..",
        "answer_c": "cd .",
        "answer_d": "cd\/",
        "answer_e": "None of the above",
        "answer_f": "cd.."
    },
    "multiple_correct_answers": "false",
    "correct_answers": {
        "answer_a_correct": "false",
        "answer_b_correct": "true",
        "answer_c_correct": "false",
        "answer_d_correct": "false",
        "answer_e_correct": "false",
        "answer_f_correct": "false"
    },
    "correct_answer": "answer_a",
    "explanation": null,
    "tip": null,
    "tags": [{
        "name": "BASH"
    }],
    "category": "Linux",
    "difficulty": "Easy"
}, {
    "id": 717,
    "question": "Which command is used to create a deployment in Kubernetes?",
    "description": null,
    "answers": {
        "answer_a": "kubectl run",
        "answer_b": "kubernetes set deployment",
        "answer_c": "kubectl deploy",
        "answer_d": "kubectl apply deployment",
        "answer_e": null,
        "answer_f": null
    },
    "multiple_correct_answers": "false",
    "correct_answers": {
        "answer_a_correct": "true",
        "answer_b_correct": "false",
        "answer_c_correct": "false",
        "answer_d_correct": "false",
        "answer_e_correct": "false",
        "answer_f_correct": "false"
    },
    "correct_answer": "answer_a",
    "explanation": null,
    "tip": null,
    "tags": [{
        "name": "Kubernetes"
    }],
    "category": "Linux",
    "difficulty": "Easy"
}, {
    "id": 1068,
    "question": "What will happen If you run the command \"init 6\" in your terminal",
    "description": null,
    "answers": {
        "answer_a": "Reboot the system",
        "answer_b": "Shut down the system",
        "answer_c": "Enter single user mode",
        "answer_d": "Start the system without a display manager (GUI)",
        "answer_e": null,
        "answer_f": null
    },
    "multiple_correct_answers": "false",
    "correct_answers": {
        "answer_a_correct": "true",
        "answer_b_correct": "false",
        "answer_c_correct": "false",
        "answer_d_correct": "false",
        "answer_e_correct": "false",
        "answer_f_correct": "false"
    },
    "correct_answer": "answer_a",
    "explanation": null,
    "tip": null,
    "tags": [{
        "name": "Linux"
    }, {
        "name": "BASH"
    }],
    "category": "Linux",
    "difficulty": "Medium"
}, {
    "id": 648,
    "question": "Which command can be used to determine file type by its content?",
    "description": null,
    "answers": {
        "answer_a": "file",
        "answer_b": "ls \u2013l",
        "answer_c": "type",
        "answer_d": "None of the above.",
        "answer_e": null,
        "answer_f": null
    },
    "multiple_correct_answers": "false",
    "correct_answers": {
        "answer_a_correct": "true",
        "answer_b_correct": "false",
        "answer_c_correct": "false",
        "answer_d_correct": "false",
        "answer_e_correct": "false",
        "answer_f_correct": "false"
    },
    "correct_answer": "answer_a",
    "explanation": null,
    "tip": null,
    "tags": [{
        "name": "BASH"
    }],
    "category": "Linux",
    "difficulty": "Easy"
}, {
    "id": 720,
    "question": "Cronjobs in kubernetes run in",
    "description": null,
    "answers": {
        "answer_a": "UTC only",
        "answer_b": "Based on NTP settings",
        "answer_c": "Master node local timezone",
        "answer_d": "GMT only",
        "answer_e": null,
        "answer_f": null
    },
    "multiple_correct_answers": "false",
    "correct_answers": {
        "answer_a_correct": "true",
        "answer_b_correct": "false",
        "answer_c_correct": "false",
        "answer_d_correct": "false",
        "answer_e_correct": "false",
        "answer_f_correct": "false"
    },
    "correct_answer": "answer_a",
    "explanation": null,
    "tip": null,
    "tags": [{
        "name": "Kubernetes"
    }],
    "category": "Linux",
    "difficulty": "Easy"
}, {
    "id": 32,
    "question": "Which command is used to display the default permissions for newly created files?",
    "description": null,
    "answers": {
        "answer_a": "umask",
        "answer_b": "priority",
        "answer_c": "nice",
        "answer_d": "perm",
        "answer_e": null,
        "answer_f": null
    },
    "multiple_correct_answers": "false",
    "correct_answers": {
        "answer_a_correct": "true",
        "answer_b_correct": "false",
        "answer_c_correct": "false",
        "answer_d_correct": "false",
        "answer_e_correct": "false",
        "answer_f_correct": "false"
    },
    "correct_answer": "answer_a",
    "explanation": null,
    "tip": null,
    "tags": [{
        "name": "Linux"
    }],
    "category": "Linux",
    "difficulty": "Medium"
}, {
    "id": 674,
    "question": "Which of the following command can give documentation of a command?",
    "description": null,
    "answers": {
        "answer_a": "man",
        "answer_b": "info",
        "answer_c": "doc",
        "answer_d": "show",
        "answer_e": "help",
        "answer_f": null
    },
    "multiple_correct_answers": "false",
    "correct_answers": {
        "answer_a_correct": "true",
        "answer_b_correct": "false",
        "answer_c_correct": "false",
        "answer_d_correct": "false",
        "answer_e_correct": "false",
        "answer_f_correct": "false"
    },
    "correct_answer": "answer_a",
    "explanation": null,
    "tip": null,
    "tags": [{
        "name": "BASH"
    }, {
        "name": "Linux"
    }],
    "category": "Linux",
    "difficulty": "Easy"
}, {
    "id": 13,
    "question": "What command, followed by the directory name is used to access that specific directory?",
    "description": null,
    "answers": {
        "answer_a": "cp",
        "answer_b": "cd",
        "answer_c": "access",
        "answer_d": "acs",
        "answer_e": null,
        "answer_f": null
    },
    "multiple_correct_answers": "false",
    "correct_answers": {
        "answer_a_correct": "false",
        "answer_b_correct": "true",
        "answer_c_correct": "false",
        "answer_d_correct": "false",
        "answer_e_correct": "false",
        "answer_f_correct": "false"
    },
    "correct_answer": "answer_b",
    "explanation": null,
    "tip": null,
    "tags": [{
        "name": "Linux"
    }],
    "category": "Linux",
    "difficulty": "Easy"
}];

var array = [];
for (let i = 0; i < json.length; i++) {
    var result = Object.keys(json[i]['answers']).map(function (key) {
        return json[i]['answers'][key];
    });
    array.push(new Question(json[i].question, result, json[i].answers[json[i].correct_answer]));
}
var quiz = new Quiz(array);
populate();