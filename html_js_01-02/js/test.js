var Form = function(params) {
    this.el = params.el;
    this.rule = params.rules;
    this.listItem = [];

    this.init = function() {
        var self = this;
        //console.log(this.rule);
        // sử dụng vòng lặp for in để duyệt mảng object
        // Nên tìm hiểu nhiều vòng lặp và tra google.
        //console.log(this.rule);
        for (let i in this.rule) {
            let Item = new InputItem({ el: this.rule[i].el, rule: this.rule[i].rule, messege: this.rule[i].messege });
            this.listItem.push(Item);
        }
        //console.log(this.listItem);
        // self.validation();
    }
    this.init();
};


var InputItem = function(params) {
    this.el = params.el;
    this.rule = params.rule;
    this.messege = params.messege;

    this.submit = 'submit';
    this.reset = 'resetForm';
    // {required : true, type: email

    this.init = function() {
        //console.log(this.el);

        var self = this;
        //console.log(this.messege);

        document.getElementById(this.submit).addEventListener('click', function(e) {
            if (self.validation() == true) {
                self.add();
            } else {
                self.printError();
            }

            self.add();
        }, true);
    }
    this.init();
}

// hàm split là phương thức tách chuỗi và đưa vào mảng array

InputItem.prototype.validation = function() {

    let rule = this.rule.split('|');


    const Getvalue = document.getElementById(this.el);

    //const Textid = document.getElementById();

    //console.log("hi dong RA ĐI" + messege);
    // console.log("co hay ko" + rule[2]);
    //alert("xin chao " + rule[2]);
    if (rule[0] == 'required') {
        if (Getvalue.value == '') {
            document.getElementById('idForm').classList.remove("invalid");
            document.getElementById('idForm').classList.add("valid");
            document.getElementById(this.messege).innerHTML = "Bạn chưa nhập giá trị vui lòng nhập lại";
            document.getElementById(this.messege).style.color = "#ff0000";
        } else {
            document.getElementById('idForm').classList.add("valid");
            document.getElementById('idForm').classList.remove("invalid");
            document.getElementById(this.messege).innerHTML = "Đã được nhập giá trị";
            document.getElementById(this.messege).style.color = "#00ff00";

        }
    }

    switch (rule[1]) {
        case 'email':
            return this.validaterEmail(document.getElementById(this.el).value, document.getElementById(this.messege));
            break;
        case 'password':
            return this.validaterRepeatPasswrod(document.getElementById(this.el).value, document.getElementById(this.messege));
            break;
    }

    return true;
}

InputItem.prototype.printError = function(name) {

    if (this.validaterRepeatPasswrod() == false) {
        console.log("password không trùng nhau");
    }

}

InputItem.prototype.validaterEmail = function(email, text2) {
    //function

    pattern = /^[^ ]+@[^ ]+\.[a-z]{2,3}$/;
    if (email.match(pattern)) {
        document.getElementById('idForm').classList.add("valid");
        document.getElementById('idForm').classList.remove("invalid");
        text2.innerHTML = "Emai của bạn đã đúng";
        text2.style.color = "#00ff00";

    } else {
        document.getElementById('idForm').classList.remove("invalid");
        document.getElementById('idForm').classList.add("valid");
        text2.innerHTML = "Emai của bạn nhập sai";
        text2.style.color = "#ff0000";

    }

}


InputItem.prototype.validaterRepeatPasswrod = function(repass, Text4) {

    if (repass === document.getElementById('inputPassword').value) {
        document.getElementById('idForm').classList.add("valid");
        document.getElementById('idForm').classList.remove("invalid");
        Text4.innerHTML = "Mật khẩu trùng khớp";
        Text4.style.color = "#00ff00";
    } else {
        document.getElementById('idForm').classList.remove("invalid");
        document.getElementById('idForm').classList.add("valid");
        Text4.innerHTML = "Mật khẩu không trùng khớp";
        Text4.style.color = "#ff0000";
    }
}

Form.prototype.validation = function() { // return true/false
    let valid = true;
    for (let i = 0; i < this.listItem.length; i++) {
        this.listItem[i].validation();
    }
    return valid;
}

InputItem.prototype.add = function() {
    var now = new Date();
    now.setMonth(now.getMonth() + 1);
    cookievalue = escape(document.getElementById(this.el).value) + ";"

    document.cookie = "name=" + cookievalue;
    document.cookie = "expires=" + now.toUTCString() + ";"
    document.write("Setting Cookies : " + "name=" + cookievalue);

    document.cookie.split(';');
}



var form1 = new Form({
    el: '#idForm',
    rules: {
        usename: { el: 'inputUser', rule: 'required', messege: 'text1' },
        email: { el: 'inputEmail', rule: 'required|email', messege: 'text2' },
        password: { el: 'inputPassword', rule: 'required', messege: 'text3' },
        repeat_password: { el: 'inputRepeatpassword', rule: 'required|password', messege: 'text4' },
        name: { el: 'inputName', rule: 'required', messege: 'text5' },
        address: { el: 'inputAddress', rule: '', messege: 'text6' },
        bio: { el: 'inputBio', rule: '', messege: 'tex7' },

    },

});


//console.log(Object.keys(InputItem));