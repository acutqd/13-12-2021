var mang_loi = [];
var thongbaoloi = {
    'username': ' tên đăng nhập',
    'password': ' mật khẩu',
    're_password': ' nhập lại mật khẩu',
    'name': ' tên',
    'email': ' địa chỉ email'
};
var Form = function (params) {
    this.el = params.el;
    this.rules = params.rules;
    this.items = [];
    this.element = null;
    this.init = function () {
        this.element = document.getElementById(this.el);
        if (this.element == null) throw new Error('From element doesn\'t exist!');
        for (let item in this.rules) {
            let itemElement = this.element.querySelector('input[name="' + item + '"]');
            if (itemElement == null) continue;
            let itemInput = new Item({
                el: itemElement,
                rules: this.rules[item]
            });
            this.items.push(itemInput);

        }
    }
    this.init();
}

Form.prototype.validation = function () {
    let error = 0;
    for (let item in this.items) {
        if (!this.items[item].validation()) error++;
    }
    if (error > 0) return false;
    return true;
}

var Item = function (params) {
    this.element = params.el;
    this.rules = params.rules;
    var self = this;
    this.element.addEventListener('blur', function (event) {
        self.validation();
        if (self.validation() == true) {
            self.addCokie();
        }
    });
}

Item.prototype.validation = function () {
    let ruleLength = Object.keys(this.rules).length;
    let check = 0;
    var itemElement = this.element;
    for (let rule in this.rules) {
        switch (rule) {
            case 'required':
                if (this.required(itemElement.value)) check++;
                break;
            case 'email':
                if (this.email(itemElement.value)) check++;
                break;
            case 'dodai':
                if (this.dodai(itemElement.value, this.rules[rule])) check++;
                break;
            case 'equalTo':
                if (this.equalTo(itemElement.value, this.rules[rule])) check++;
                break;
        }
    }
    if (check < ruleLength) {
        this.printError();
        return false;
    } else {
        this.removeError();
        return true;
    }
}

function showError(key, mess) {
    var get_element = document.getElementById(key + '_error');
    get_element.innerHTML = "<span style='position: absolute;top:0; color:red;'>" + mess + "</span>";
}
Item.prototype.printError = function () {


    var x = document.getElementById(this.element.id).nextSibling;
    var x = this.element.id;
    showError(x, mang_loi[x]);
    this.element.style.borderColor = 'red';
    // console.log(mang_loi);
}

Item.prototype.removeError = function () {
    var x = document.getElementById(this.element.id).nextSibling;
    var x = this.element.id;
    this.element.style.borderColor = '';
    showError(x, '');
}
Item.prototype.required = function (value) {
    if (value == '') {
        mang_loi[this.element.id] = 'Không được để trống ' + thongbaoloi[this.element.name];
        // console.log(mang_loi)
        return false;
    }

    return true;
}
Item.prototype.dodai = function (value, length) {
    if (value == '') {
        mang_loi[this.element.id] = 'Không được để trống' + thongbaoloi[this.element.name];
        return false;
    } else if (value.length < length) {
        mang_loi[this.element.id] = 'Độ dài ' + thongbaoloi[this.element.name] + ' phải lớn hơn ' + length;
        return false;
    }
    return true;
}
Item.prototype.email = function (value) {
    let regex = new RegExp('[a-z0-9]+@[a-z]+\.[a-z]{2,3}');
    if (value == '') {
        mang_loi[this.element.id] = 'Không được để trống' + thongbaoloi[this.element.name];
        return regex.test(value);
    } else if (regex.test(value) != true) {
        mang_loi[this.element.id] = 'Email nhập không đúng định dạng ' + this.element.name;
        return regex.test(value);
    } else {
        return regex.test(value);
    }
}


Item.prototype.equalTo = function (value, equalToEl) {
    var equalElement = document.querySelector('input[name="' + equalToEl + '"]');
    if (equalElement.value === value) {
        return true;
    } else {
        mang_loi[this.element.id] = 'Nhập lại mật khẩu không đúng ';
        return false;
    }
}

Item.prototype.file = function (value) {
    //
}


Item.prototype.addCokie = function () {
    var now = new Date();
    now.setMonth(now.getMonth() + 1);
    cookievalue = escape(document.getElementById(this.el).value) + ";"

    document.cookie = "name=" + cookievalue;
    document.cookie = "expires=" + now.toUTCString() + ";"
    document.write("Setting Cookies : " + "name=" + cookievalue);

    document.cookie.split(';');
}